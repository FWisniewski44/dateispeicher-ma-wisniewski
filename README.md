# Speicher-Repository der Masterarbeit "Twitter-Deutschland zwischen Medien und Politik. Eine datenintensive, computerbasierte Untersuchung der wechselseitigen Beziehung medialer und politischer Akteure im deutschen Twitter-Kosmos"

## Allgemeine Informationen

Dieses Repository ist der an der Otto-Friedrich-Universität Bamberg von Florian Wisniewski eingereichten Masterarbeit mit dem obigen Titel zuzuordnen.

Es ergänzt das Code-Repository auf GitHub, welches unter diesem Link aufrufbar ist: https://github.com/FWisniewski44/polResp-css

Die CSV-Dateien wurden mittels Git Large File Storage hochgeladen. Weitere Informationen hierzu unter: https://git-lfs.com/

## Informationen zu Ordnerstruktur

+ Der Ordner *mediendaten* enthält sämtliche relevante Datensätze, die mit der Akteurssphäre der Medien in Verbindung stehen im CSV-Format.

+ Der Ordner *politikerdaten* enthält sämtliche relevante Datensätze, die mit der Akteurssphäre der PolitikerInnen in Verbindung stehen im CSV-Format.

+ Im Ordner *demojized* befinden sich jene Datensätze im CSV-Format für die beiden Akteurssphären, die die mittels Python bereinigten Tweets beinhalten. Zu diesen wurden noch keine Themen zugewiesen.

+ Im Ordner *ausw-caoJuan-Deveaud* befinden sich Datensätze im RData-Format für die jeweilige Akteurssphäre, in welchen die jeweiligen Koeffizienten-Werte zur Bestimmung idealer k-Werte für die Topic Models enthalten sind.

+ Der Ordner *terms-lda* beinhaltet die Wortlisten mit den durch die LDA zusammensortierten Wörtern pro Thema im CSV-Format.

## Nähere Erklärungen und Zusammenhänge

+ Dateien im Ordner *demojized* wurden genutzt, um die Topic Models zu erstellen

+ Ergebnisse der Topic Models finden sich in *terms-lda*

+ Ebenso wurden die Dateien in *demojized* zur Zuweisung der Tweets zu den Themen genutzt

+ Ergebnis dessen sind die Dateien in *mediendaten* und *politikerdaten*
